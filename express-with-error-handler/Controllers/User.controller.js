const createError = require('http-errors');

const User = require('../Models/User.model');
const {userValidate} = require('../helpers/validation');
const {signAccessToken, signRefreshToken, verifyRefreshToken} = require('../helpers/jwt_service');
const client = require('../helpers/connection_redis');

module.exports = {
    register: async (req, res, next) => {
        try {
            const {email, password} = req.body;
    
            const {error} = userValidate(req.body);
    
            if(error) {
                throw createError(error.details[0].message);
            }
    
            const isExists =  await User.findOne({
                username: email
            });
    
            if(isExists) {
                throw createError.Conflict(`${email} is already been registed`);
            }
    
            const user = new User({
                username: email, 
                password: password
            })
    
            const savedUser = await user.save();
    
            return res.json({
                status: 'OK',
                elements: savedUser
            })
        } catch (err) {
            next(err);
        }
    },


    refreshToken: async (req, res, next) => {
        try {
            if(!req.body.refreshToken) {
                throw createError.BadRequest();
            }
            const {userId} = await verifyRefreshToken(req.body.refreshToken);
            const accessToken = await signAccessToken(userId);
            const refreshToken = await signRefreshToken(userId);
            res.json({
                accessToken,
                refreshToken
            })
        } catch (err) {
            next(err);
        }
    },


    login: async (req, res, next) => {
        try {   
            const {error} = userValidate(req.body);
    
            if(error) {
                throw createError(error.details[0].message);
            }
    
            const user = await User.findOne({
                username: req.body.email
            })
    
            if(!user) {
                throw createError.NotFound('User not registed')
            }
    
            const isValid = await user.isCheckPassword(req.body.password);
            if(!isValid) {
                throw createError.Unauthorized();
            }
            
            const accessToken = await signAccessToken(user._id);
            const refreshToken = await signRefreshToken(user._id);
            res.json({
                accessToken,
                refreshToken
            })
        } catch (err) {
            next(err);
        }
    },


    logout: async (req, res, next) => {
        try {
            if(!req.body.refreshToken) {
                throw createError.BadRequest();
            }
            const {userId} = await verifyRefreshToken(req.body.refreshToken);
            client.del(userId.toString())
                .then(result => {
                    res.json({
                        message: 'Logout!'
                    })
                })
                .catch(err => {
                    throw createError.InternalServerError();
                })
        } catch (err) {
            next(err)
        }
    },


    getLists: (req, res, next) => {
        const listUsers = [
            {
                email: 'abc@gmail.com'
            },
            {
                email: 'def@gmail.com'
            }
        ]
    
        res.json({
            listUsers
        });
    }
}
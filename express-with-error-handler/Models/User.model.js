const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const schema = mongoose.Schema;

const {testConnection} = require('../helpers/connection_multi_mongodb');

const UserSchema = new schema({
    username: {
        type: String, 
        lowercase: true,
        unique: true, 
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

UserSchema.pre('save', async function(next) {
    try {
        const salt = await bcrypt.genSalt(10);  // Hash password with some salt ==> same password but different hash
        const hashedPassword = await bcrypt.hash(this.password, salt);
        this.password = hashedPassword;
        next();
    } catch (err) {
        next(err)
    }
})

UserSchema.methods.isCheckPassword = async function(password) {
    try {
        return await bcrypt.compare(password, this.password);
    } catch (err) {
        next(err)
    }
}

module.exports = testConnection.model('user', UserSchema);
const redis = require('redis');
const client = redis.createClient();

async function connectRedis(client) {
    await client.connect();
}

connectRedis(client);

client.on("error", function(error) {
    console.error(error);
})

client.on("connect", function(error) {
    console.log("connected");
})

client.on("ready", function(error) {
    console.log("Redis to ready"); 
})

module.exports = client;
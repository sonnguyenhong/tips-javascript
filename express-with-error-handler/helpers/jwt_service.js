const JWT = require('jsonwebtoken');
const createError = require('http-errors');
const client = require('./connection_redis');

const signAccessToken = async (userId) => {
    return new Promise((resolve, reject) => {
        const payload = {
            userId
        }

        const secret = process.env.ACCESS_TOKEN_SECRET
        const options = {
            expiresIn: process.env.ACCESS_TOKEN_EXPIRES_TIME
        }

        JWT.sign(payload, secret, options, (err, token) => {
            if(err) {
                reject(err);
            }
            resolve(token);
        })
    })
}

const signRefreshToken = async (userId) => {
    return new Promise((resolve, reject) => {
        const payload = {
            userId
        }

        const secret = process.env.REFRESH_TOKEN_SECRET;
        const options = {
            expiresIn: process.env.REFRESH_TOKEN_EXPIRES_TIME
        }

        JWT.sign(payload, secret, options, (err, token) => {
            if(err) {
                reject(err);
            }
            client.set(userId.toString(), token, {
                EX: 365*24*60*60
            })
            resolve(token);
        })
    })
}

const verifyAccessToken = (req, res, next) => {
    if(!req.headers['authorization']) {
        return next(createError.Unauthorized())
    }
    const authHeader = req.headers['authorization'];
    const token = authHeader.split(' ')[1];
    JWT.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
        if(err) {
            if(err.name === "JsonWebTokenError") {
                return next(createError.Unauthorized())
            }
            return next(createError.Unauthorized(err.message))
        }
        req.payload = payload;
        next();
    })
}

const verifyRefreshToken = (refreshToken) => {
    return new Promise((resolve, reject) => {
        JWT.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, async (err, payload) => {
            if(err) {
                console.log('Error');
                reject(err);
            }

            const refToken = await client.get(payload.userId);
            
            if(refreshToken === refToken) {
                return resolve(payload);
            }
            return reject(createError.Unauthorized());
        })
    })
}

module.exports = {
    signAccessToken,
    signRefreshToken,
    verifyAccessToken,
    verifyRefreshToken
}
const express = require('express');
const createError = require('http-errors');
require('dotenv').config();

const UserRoute = require('./Routes/User.route');
const client = require('./helpers/connection_redis');
require('./helpers/connection_mongodb');

client.set("foo", "hongson1");

const getKey = async (key) => {
    const value = await client.get("foo");
    console.log(value);
}

getKey("foo");


app = express();

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))

app.get('/', (req, res, next) => {
    res.send('Home page')
})

app.use('/user', UserRoute);

app.use((req, res, next) => {
    // const error = new Error('Not found');
    // error.status = 500;
    // next(error);
    next(createError.NotFound('This route does not exist'));
})

app.use((err, req, res, next) => {
    res.json({
        status: err.status || 500,
        message: err.message
    })
})

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`);
})
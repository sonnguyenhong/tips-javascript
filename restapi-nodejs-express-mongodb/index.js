const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const createError = require('http-errors');
const {v4: uuid} = require('uuid');
require('dotenv').config();

require('./helpers/connection_mongodb');
const logEvents = require('./helpers/log_events');

const userRoutes = require('./Routes/User.route');


const app = express();
app.use(helmet());
app.use(morgan('common'));
app.use('/v1/users', userRoutes);
app.use((req, res, next) => {
    next(createError(404, "Not Found!"));
})

app.use((err, req, res, next) => {
    logEvents(`idErorr ---- ${uuid()} ---- ${req.url} ---- ${err.method} ---- ${err.message}`);
    res.status(err.status || 500);
    res.json({
        status: err.status || 500,
        message: err.message
    })
})


const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server is running at ${PORT}`);
})
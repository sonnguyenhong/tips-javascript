const express = require('express');
const route = express.Router();

// Get a list of all users
route.get('/', (req, res, next) => {
    res.json({
        status: 'success',
        elements: [{}]
    })
})

module.exports = route;